# Sample test of automation for create a debian lxc from official image and import a mysql DB into a LAMP stack

## what do you need?

- proxmox VE installation
- internet connection
- a LXC/VM to use like infrastructure control center [ICC]
- ansible installed [icc]
- terraform installed [icc]
- basics files for a website and a DB dump

## what do I get at the end?

- 1 debian LXC with lamp stack with a simple website and a restored DB

## what steps are necessary?

- upload on a pve node a official LXC ubutnu template
- use terraform for create 1 LXC from the modified template
- prepare the webiste files
- prepare the DB dump
- execute ansible playbook on this LXC for create a lamp environment and import the DB

## how to get a alpine LXC template?

- `pveam update`
- `pveam available`
- `pveam download $storage_template $template_name`

## how to use terraform?

- `terraform -chdir=terraform init`
- `terraform -chdir=terraform plan`
- `terraform -chdir=terraform apply`

## how to use ansible?

- `mv /path/to/website/and/db/files ansible/files/`
- `ansible-playbook -i ansible/ansible_hosts.txt  ansible/ansible_lxc_deb_import_db.yaml`
